# README.md #
Kódování: UTF-8

Kontakt: Jan Herec, herech@seznam.cz

Repozitář sestává především z:

1) Projektu (adresář / Android Studio project / MyoControlsRobot), který je určený pro Android Studio (testováno pro verze: Android Studio 1.5.1 (Ubuntu 14.04) a Android Studio 1.5.2 (Windows 7)), a který obsahuje zdrojové kódy Android aplikace navržené pro řízení pojízdného ROS robota s využitím nositelného zařízení Myo armband. Po otevření projektu v Android Studiu je možné jej dle potřeby modifikovat (především zdrojové kódy které se nacházejí v adresáři / Android Studio project / MyoControlsRobot / MyoControlsRobot / src / main / java / org / bitbucket / herech / myo_controlled_ros_robot) a také přeložit.

2) Soubor typu .apk, který představuje instalační soubor mobilní aplikace. Aplikaci je možné nainstalovat a používat na Android zařízeních (verze OS Android >= 4.3), která podporují technologii Bluetooth LE a mají šířku displeje >= 4.11 cm. 

3) Licenčního ujednání (soubor NOTICE)

4) Manuálu (soubor MANUAL), který podává některé další užitečné informace k tomuto projektu

5) Elektronické verze BP (soubor BP.pdf), v rámci které vznikl tento projekt


([more info](http://www.fit.vutbr.cz/study/DP/BP.php.en?id=18649&y=0&st=Herec))