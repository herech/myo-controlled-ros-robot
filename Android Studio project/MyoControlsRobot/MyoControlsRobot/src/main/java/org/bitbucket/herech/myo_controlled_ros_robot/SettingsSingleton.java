/**
 *  Copyright 2016 Jan Herec, herech@seznam.cz
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *         you may not use this file except in compliance with the License.
 *         You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *        distributed under the License is distributed on an "AS IS" BASIS,
 *        WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *        See the License for the specific language governing permissions and
 *        limitations under the License.
 */

/**
 *  Created by Jan Herec on 2.3.2016 within the bachelor thesis "Wearable Devices: Possibilities of Using and Applications"
 *  at Faculty of Information Technology, Brno University of Technology.
 */

package org.bitbucket.herech.myo_controlled_ros_robot;

/**
 * Singleton stores volatile variables, which are intended to communicate between threads.
 * One thread receives data from myo and based on them sets variables,
 * another thread according these variables sends control messages to ROS robot)
 */
public class SettingsSingleton {
    // singleton instance
    private static SettingsSingleton instance = null;
    // element x of linear velocity vector
    public volatile double linearVelocityX = 0.0;
    // element z of angular velocity vector
    public volatile double angularVelocityZ = 0.0;
    // time period after which are repeatedly being sent directional messages to ROS robot
    public volatile long sleepTime;

    protected SettingsSingleton() {}

    public static SettingsSingleton getInstance() {
        if(instance == null) {
            instance = new SettingsSingleton();
        }
        return instance;
    }
}
