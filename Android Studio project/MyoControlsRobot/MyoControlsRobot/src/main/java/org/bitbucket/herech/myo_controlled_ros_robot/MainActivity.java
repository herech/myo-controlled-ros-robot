/**
 *  Copyright 2016 Jan Herec, herech@seznam.cz
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *         you may not use this file except in compliance with the License.
 *         You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *        distributed under the License is distributed on an "AS IS" BASIS,
 *        WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *        See the License for the specific language governing permissions and
 *        limitations under the License.
 */

/**
 *  Created by Jan Herec on 13.2.2016 within the bachelor thesis "Wearable Devices: Possibilities of Using and Applications"
 *  at Faculty of Information Technology, Brno University of Technology.
 *  Based on sample source code from Thalmic Labs Inc. Copyright (C) 2014. Distributed under the Myo SDK license agreement.
 */

package org.bitbucket.herech.myo_controlled_ros_robot;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.thalmic.myo.AbstractDeviceListener;
import com.thalmic.myo.Arm;
import com.thalmic.myo.DeviceListener;
import com.thalmic.myo.Hub;
import com.thalmic.myo.Myo;
import com.thalmic.myo.Pose;
import com.thalmic.myo.Quaternion;
import com.thalmic.myo.Vector3;
import com.thalmic.myo.XDirection;
import com.thalmic.myo.scanner.ScanActivity;

import org.ros.address.InetAddressFactory;
import org.ros.node.NodeConfiguration;

import org.ros.android.RosActivity;
import org.ros.node.NodeMainExecutor;


/**
 * MainActivity represents Activity which is started as first after launch Android application
 * This activity also represents main part of application with which is user in contact,
 * because is running in UI thread and take care of event handling
 */
public class MainActivity extends RosActivity {

    private ROSMessageSenderNode rosMessageSenderNode; // instance of ROS Node, which sends control messages to ROS node on robot
    private NodeMainExecutor ROSnodeMainExecutor;      // instance of class, which executes in background thread code in ROSMessageSenderNode

    private TextView mMyoLockStateView; // represents text field on screen, which show state of Myo (locked/unlocked)
    private TextView mMyoStateView;     // represents text field on screen, which show state of Myo (current gesture, disconnect, ...)

    private double basicLinearVelocity;  // basic linear velocity which is set by user
    private double basicAngularVelocity; // basic angular velocity which is set by user
    
    private boolean advancedControlModeEnabled;                                         // flag, that indicates whenever user has allowed advanced control mode
    private boolean experimentalControlModeEnabled;                                     // flag, that indicates whenever user has allowed experimental control mode
    private boolean fistGesturePerformedWithinExperimentalControlMode = false;          // flag, that indicates whenever fist gesture was performed within experimental control mode
    private boolean fingersSpreadGesturePerformedWithinExperimentalControlMode = false; // flag, that indicates if fingers spread gesture was performed within experimental control mode

    /**
     * Classes that inherit from AbstractDeviceListener can be used to receive events from Myo devices
     * If you do not override an event, the default behavior is to do nothing.
     */
    private DeviceListener mListener = new AbstractDeviceListener() {

        private boolean isMyoUnlocked = false;                   // flag, that indicates whenever gesture recognition is locked
        private boolean needToRememberCurrentPitchValue = false; // flag, that indicates whenever is necessary to temporarily remember current pitch value
        private float rememberedYawValue = 0.0f;                 // temporarily remembered yaw value
        private float previousYawValue = 0.0f;                   // temporarily remembered previous/last yaw value
        private float rememberedPitchValue = 0.0f;               // temporarily remembered pitch value
        private boolean needToRememberCurrentYawValue = false;   // flag, that indicates whenever is necessary to temporarily remember current yaw value
        private boolean canApplyAdvancedControlMode = false;     // flag, that indicates whenever is possible apply advanced control
        private boolean robotMovesBackward = false;              // flag, that indicates whenever robot has to move backward or forward

        /**
         * onConnect() is called whenever a Myo has been connected.
         */
        @Override
        public void onConnect(Myo myo, long timestamp) {
            // inform user that Myo has been connected
            mMyoStateView.setTextColor(Color.CYAN);
            mMyoStateView.setText(R.string.myo_connected);
        }

        /**
         * onDisconnect() is called whenever a Myo has been disconnected.
         */
        @Override
        public void onDisconnect(Myo myo, long timestamp) {
            // inform user that Myo has been disconnected
            mMyoStateView.setTextColor(Color.parseColor("#e53935"));
            mMyoStateView.setText(R.string.myo_disconnected);
            mMyoStateView.setRotation(0f);
            mMyoStateView.setRotationX(0f);
            mMyoStateView.setRotationY(0f);

            // reset angluar and linear velocity and other state variables
            SettingsSingleton.getInstance().linearVelocityX = 0.0;
            SettingsSingleton.getInstance().angularVelocityZ = 0.0;
            canApplyAdvancedControlMode = false;
            fistGesturePerformedWithinExperimentalControlMode = false;
            fingersSpreadGesturePerformedWithinExperimentalControlMode = false;
            this.needToRememberCurrentPitchValue = false;
            this.needToRememberCurrentYawValue = false;
        }

        /**
         * onArmSync() is called whenever Myo has recognized a Sync Gesture after someone has put it on their
         * arm. This lets Myo know which arm it's on and which way it's facing.
         */
        @Override
        public void onArmSync(Myo myo, long timestamp, Arm arm, XDirection xDirection) {
            // inform user that myo is synchronized (on left or right arm)
            mMyoStateView.setText(myo.getArm() == Arm.LEFT ? R.string.arm_left : R.string.arm_right);
        }

        /**
         * onArmUnsync() is called whenever Myo has detected that it was moved from a stable position on a person's arm after
         * it recognized the arm. Typically this happens when someone takes Myo off of their arm, but it can also happen
         * when Myo is moved around on the arm.
         */
        @Override
        public void onArmUnsync(Myo myo, long timestamp) {
            // inform user that Myo is connected, but not synchronized
            mMyoStateView.setText(R.string.myo_connected);

            // reset angluar and linear velocity and other state variables
            SettingsSingleton.getInstance().linearVelocityX = 0.0;
            SettingsSingleton.getInstance().angularVelocityZ = 0.0;
            canApplyAdvancedControlMode = false;
            fistGesturePerformedWithinExperimentalControlMode = false;
            fingersSpreadGesturePerformedWithinExperimentalControlMode = false;
            this.needToRememberCurrentPitchValue = false;
            this.needToRememberCurrentYawValue = false;
        }

        /**
         * onUnlock() is called whenever a synced Myo has been unlocked. Under the standard locking policy, 
         * that means poses will now be delivered to the listener.
         */
        @Override
        public void onUnlock(Myo myo, long timestamp) {
            // inform user that Myo is unlocked
            mMyoLockStateView.setText(R.string.unlocked);
            mMyoLockStateView.setTextColor(Color.parseColor("#00c853"));
            this.isMyoUnlocked = true;
            // myo stay unlocked until is locked by gesture double tap
            myo.unlock(Myo.UnlockType.HOLD);
        }

        /**
         * onLock() is called whenever a synced Myo has been locked. Under the standard locking policy, 
         * that means poses will no longer be delivered to the listener.
         */
        @Override
        public void onLock(Myo myo, long timestamp) {
            // inform user that Myo is locked
            mMyoLockStateView.setText(R.string.locked);
            mMyoLockStateView.setTextColor(Color.parseColor("#FFBDBDBD"));

            // reset angluar and linear velocity and other state variables
            SettingsSingleton.getInstance().linearVelocityX = 0.0;
            SettingsSingleton.getInstance().angularVelocityZ = 0.0;
            canApplyAdvancedControlMode = false;
            fistGesturePerformedWithinExperimentalControlMode = false;
            fingersSpreadGesturePerformedWithinExperimentalControlMode = false;
            this.needToRememberCurrentPitchValue = false;
            this.needToRememberCurrentYawValue = false;
        }

        /**
         * onOrientationData() is called whenever a Myo provides its current orientation, represented as a quaternion.
         */
        @Override
        public void onOrientationData(Myo myo, long timestamp, Quaternion rotation) {
            // Calculate Euler angles (roll, pitch, and yaw) from the quaternion.
            float roll = (float) Math.toDegrees(Quaternion.roll(rotation));
            float pitch = (float) Math.toDegrees(Quaternion.pitch(rotation));
            float yaw = (float) Math.toDegrees(Quaternion.yaw(rotation));

            // Adjust roll and pitch for the orientation of the Myo on the arm.
            if (myo.getXDirection() == XDirection.TOWARD_ELBOW) {
                roll *= -1;
                pitch *= -1;
            }

            // if advanced control mode is enabled by user and could be also applied (user is holding or has performed gesture fist or fingers spread)
            if (advancedControlModeEnabled && canApplyAdvancedControlMode) {

                // after user has performed gesture fist or fingers spread we need remember current myo position/orientation (pitch and yaw values)
                if (this.needToRememberCurrentYawValue && this.needToRememberCurrentPitchValue) {

                    this.rememberedYawValue = yaw;
                    this.previousYawValue = this.rememberedYawValue;
                    this.rememberedPitchValue = pitch;
                    this.needToRememberCurrentYawValue = false;
                    this.needToRememberCurrentPitchValue = false;
                }

                // set properly direction of move (forward (+) or backward (-))
                double backwardDirectionConst = 1.0;
                if (robotMovesBackward == true) {
                    backwardDirectionConst = -1.0;
                }

                // I do not lift and fall my hand too much, linear velocity is basic
                if ((pitch >= (this.rememberedPitchValue - 10f)) && (pitch <= (this.rememberedPitchValue + 10f))) {
                    SettingsSingleton.getInstance().linearVelocityX = basicLinearVelocity * backwardDirectionConst;
                }
                // I lift my hand, linear velocity is increased (> basic)
                else if ((pitch < (this.rememberedPitchValue - 10f)) && (pitch >= (this.rememberedPitchValue - 20f))) {
                    SettingsSingleton.getInstance().linearVelocityX = basicLinearVelocity * 1.3 * backwardDirectionConst;
                }
                else if ((pitch <= (this.rememberedPitchValue - 20f)) && (pitch >= (this.rememberedPitchValue - 30f))) {
                    SettingsSingleton.getInstance().linearVelocityX = basicLinearVelocity * 1.6 * backwardDirectionConst;
                }
                else if (pitch <= (this.rememberedPitchValue - 30f)) {
                    SettingsSingleton.getInstance().linearVelocityX = basicLinearVelocity * 1.9 * backwardDirectionConst;
                }
                // I fall my hand, linear velocity is decreased (< basic )
                else if ((pitch > (this.rememberedPitchValue + 10f)) && (pitch <= (this.rememberedPitchValue + 20f))) {
                    SettingsSingleton.getInstance().linearVelocityX = basicLinearVelocity * 0.7 * backwardDirectionConst;
                }
                else if ((pitch >= (this.rememberedPitchValue + 20f)) && (pitch <= (this.rememberedPitchValue + 30f))) {
                    SettingsSingleton.getInstance().linearVelocityX = basicLinearVelocity * 0.3 * backwardDirectionConst;
                }
                else if (pitch >= (this.rememberedPitchValue + 30f)) {
                    SettingsSingleton.getInstance().linearVelocityX = basicLinearVelocity * 0.1 * backwardDirectionConst;
                }
                // adjustment due to the fact that we have a range of yaw from 180 degree to -180 degree.
                // And so value 179 is followed by value -179 when we turn hand left and vice versa when we turn hand right.
                // So for example: instead of sequence of values 179, -179, -178 when we turn left we make little adjustment
                // and new values are 179, 180, 181 as we expect
                if (this.previousYawValue > 140 && yaw < 0) {
                    yaw = 180f + (180f - yaw);
                }
                else if (this.previousYawValue < -140 && yaw > 0) {
                    yaw = -180f - (180f - yaw);
                }
                // I do not turn left and right my hand too much, angular velocity is null
                if ((yaw >= (this.rememberedYawValue - 10f)) && (yaw <= (this.rememberedYawValue + 10f))) {
                    SettingsSingleton.getInstance().angularVelocityZ = 0.0;
                }

                // I turn left my hand a little bit, angular velocity is basic
                else if ((yaw > (this.rememberedYawValue + 10f)) && (yaw <= (this.rememberedYawValue + 20f))) {
                    SettingsSingleton.getInstance().angularVelocityZ = basicAngularVelocity * 1.0;
                }
                // I turn left my hand a litte bit more, angular velocity is increased (> basic)
                else if ((yaw >= (this.rememberedYawValue + 20f)) && (yaw <= (this.rememberedYawValue + 30f))) {
                    SettingsSingleton.getInstance().angularVelocityZ = basicAngularVelocity * 1.3;
                }
                else if ((yaw >= (this.rememberedYawValue + 30f)) && (yaw <= (this.rememberedYawValue + 40f))) {
                    SettingsSingleton.getInstance().angularVelocityZ = basicAngularVelocity * 1.6;
                }
                else if (yaw >= (this.rememberedYawValue + 40f)) {
                    SettingsSingleton.getInstance().angularVelocityZ = basicAngularVelocity * 1.9;
                }

                // I turn right my hand a little bit, angular velocity is basic
                else if ((yaw < (this.rememberedYawValue - 10f)) && (yaw >= (this.rememberedYawValue - 20f))) {
                    SettingsSingleton.getInstance().angularVelocityZ = -basicAngularVelocity * 1.0;
                }
                // I turn right my hand a litte bit more, angular velocity is increased (> basic)
                else if ((yaw <= (this.rememberedYawValue - 20f)) && (yaw >= (this.rememberedYawValue - 30f))) {
                    SettingsSingleton.getInstance().angularVelocityZ = -basicAngularVelocity * 1.3;
                }
                else if ((yaw <= (this.rememberedYawValue - 30f)) && (yaw >= (this.rememberedYawValue - 40f))) {
                    SettingsSingleton.getInstance().angularVelocityZ = -basicAngularVelocity * 1.6;
                }
                else if (yaw <= (this.rememberedYawValue - 40f)) {
                    SettingsSingleton.getInstance().angularVelocityZ = -basicAngularVelocity * 1.9;
                }

                // current yaw value will be in next call of function onOrientationData() previous yaw value
                this.previousYawValue = yaw;
            }

            // Next, we apply a rotation to the text view using the roll, pitch, and yaw.
            // So user can check whenever orientation data are properly read by application
            mMyoStateView.setRotation(roll);
            mMyoStateView.setRotationX(pitch);
            mMyoStateView.setRotationY(yaw);

        }

        /**
         * Called when an attached Myo has provided new accelerometer data.
         */
        @Override
        public void onAccelerometerData (Myo myo, long timestamp, Vector3 accel) {
        }

        /**
         * Called when an attached Myo has provided new gyroscope data.
         */
        @Override
        public void onGyroscopeData (Myo myo, long timestamp, Vector3 gyro) {
        }

        /**
         * onPose() is called whenever a Myo provides a new pose/gesture.
         */
        @Override
        public void onPose(Myo myo, long timestamp, Pose pose) {
            // Handle the cases of the Pose enumeration, and change the text of the text view
            // based on the pose we receive.
            // According to new poses are set state variables and linear and angular velocity
            int restTextId;
            switch (pose) {
                
                case UNKNOWN:
                    mMyoStateView.setText(getString(R.string.myo_connected));

                    // reset only angular velocity just in case that fist or fingers spread gesture was performed within experimental mode, but advanced mode is disabled
                    if (fingersSpreadGesturePerformedWithinExperimentalControlMode || fistGesturePerformedWithinExperimentalControlMode) {
                        if (!advancedControlModeEnabled) {
                            SettingsSingleton.getInstance().angularVelocityZ = 0.0;
                        }
                        return;
                    }
                    // reset angular and linear velocity and other state variables
                    SettingsSingleton.getInstance().linearVelocityX = 0.0;
                    SettingsSingleton.getInstance().angularVelocityZ = 0.0;
                    canApplyAdvancedControlMode = false;
                    fistGesturePerformedWithinExperimentalControlMode = false;
                    fingersSpreadGesturePerformedWithinExperimentalControlMode = false;
                    this.needToRememberCurrentPitchValue = false;
                    this.needToRememberCurrentYawValue = false;
                    break;
                
                // rest gesture, (hand went into relaxed state)
                case REST:
                    restTextId = R.string.myo_connected;
                    switch (myo.getArm()) {
                        case LEFT:
                            restTextId = R.string.arm_left;
                            break;
                        case RIGHT:
                            restTextId = R.string.arm_right;
                            break;
                    }
                    mMyoStateView.setText(getString(restTextId));

                    // reset only angular velocity just in case that fist or fingers spread gesture was performed within experimental mode, but advanced mode is disabled
                    if (fingersSpreadGesturePerformedWithinExperimentalControlMode || fistGesturePerformedWithinExperimentalControlMode) {
                        if (!advancedControlModeEnabled) {
                            SettingsSingleton.getInstance().angularVelocityZ = 0.0;
                        }
                        return;
                    }
                    
                    // reset angular and linear velocity and other state variables
                    SettingsSingleton.getInstance().linearVelocityX = 0.0;
                    SettingsSingleton.getInstance().angularVelocityZ = 0.0;
                    canApplyAdvancedControlMode = false;
                    fistGesturePerformedWithinExperimentalControlMode = false;
                    fingersSpreadGesturePerformedWithinExperimentalControlMode = false;
                    this.needToRememberCurrentPitchValue = false;
                    this.needToRememberCurrentYawValue = false;
                    break;
                
                case DOUBLE_TAP:
                    // while myo is unlocked, gesture double tap causes lock myo 
                    if (this.isMyoUnlocked == true) {
                        myo.lock();
                        this.isMyoUnlocked = false;
                        mMyoLockStateView.setText(R.string.locked);
                    }

                    mMyoStateView.setText(getString(R.string.dbl_tap));
                    break;
                
                case FIST:
                    mMyoStateView.setText(getString(R.string.pose_fist));
                    
                    // when experimental mode is active and we waitig for providing gesture fingers spread for second time to reset linear velocity, we will not handle fist gesture
                    if (fingersSpreadGesturePerformedWithinExperimentalControlMode) return;

                    // when experimental mode is active and gesture fist was provided for second time to reset linear velocity, which was set by previous gesture fist
                    if (experimentalControlModeEnabled && fistGesturePerformedWithinExperimentalControlMode) {
                        SettingsSingleton.getInstance().linearVelocityX = 0.0;
                        fistGesturePerformedWithinExperimentalControlMode = false;
                        canApplyAdvancedControlMode = false;
                    }
                    else {
                        // when experimental mode is active and gesture fist was provided for first time to set linear velocity
                        if (experimentalControlModeEnabled) {
                            fistGesturePerformedWithinExperimentalControlMode = true;
                        }

                        // when advanced mode is active, fist gesture activate processing of orientation data in funtion onOrientationData 
                        if (advancedControlModeEnabled) {
                            this.needToRememberCurrentPitchValue = true;
                            this.needToRememberCurrentYawValue = true;
                            canApplyAdvancedControlMode = true;
                            robotMovesBackward = false;
                        }
                        // set positive linear velocity (robot moves forward)
                        SettingsSingleton.getInstance().linearVelocityX = basicLinearVelocity;
                    }
                    break;
                
                case WAVE_IN:
                    mMyoStateView.setText(getString(R.string.pose_wavein));
                    // set positive angular velocity (robot turn left)
                    SettingsSingleton.getInstance().angularVelocityZ = basicAngularVelocity;
                    break;
                
                case WAVE_OUT:
                    mMyoStateView.setText(getString(R.string.pose_waveout));
                    // set negative angular velocity (robot turn right)
                    SettingsSingleton.getInstance().angularVelocityZ = -basicAngularVelocity;
                    break;
                
                case FINGERS_SPREAD:
                    mMyoStateView.setText(getString(R.string.pose_fingersspread));
                    
                    // when experimental mode is active and we waitig for providing gesture fist for second time to reset linear velocity, we will not handle fingers spread gesture
                    if (fistGesturePerformedWithinExperimentalControlMode) return;

                    // when experimental mode is active and gesture fingers spread was provided for second time to reset linear velocity, which was set by previous gesture fingers spread
                    if (experimentalControlModeEnabled && fingersSpreadGesturePerformedWithinExperimentalControlMode) {
                        SettingsSingleton.getInstance().linearVelocityX = 0.0;
                        fingersSpreadGesturePerformedWithinExperimentalControlMode = false;
                        canApplyAdvancedControlMode = false;
                    }
                    else {
                        // when experimental mode is active and gesture fingers spread was provided for first time to set linear velocity
                        if (experimentalControlModeEnabled) {
                            fingersSpreadGesturePerformedWithinExperimentalControlMode = true;
                        }
                        
                        // when advanced mode is active, fingers spread gesture activate processing of orientation data in funtion onOrientationData
                        if (advancedControlModeEnabled) {
                            this.needToRememberCurrentPitchValue = true;
                            this.needToRememberCurrentYawValue = true;
                            canApplyAdvancedControlMode = true;
                            robotMovesBackward = true;
                        }
                        
                        // set negative linear velocity (robot moves backward)
                        SettingsSingleton.getInstance().linearVelocityX = -basicLinearVelocity;
                    }
                    break;
            }
            
            // Notify the Myo that the pose (except poses UNKNOWN and REST) has resulted in an action. The Myo will vibrate.
            if (pose != Pose.UNKNOWN && pose != Pose.REST) {
                myo.notifyUserAction();
            }
        }
    };

    /**
     * The RosActivity constructor configures the notification title and ticker messages.
     */
    public MainActivity() {
        super("Myo ROS Robot Control", "Myo ROS Robot Control");
    }

    /**
     * Throught the RosActivity function init we get nodeMainExecutor instance.
     */
    @Override
    protected void init(NodeMainExecutor nodeMainExecutor) {
        this.ROSnodeMainExecutor = nodeMainExecutor;
    }

    /**
     * Classic and basic callback function onCreate which is called after start of android application (similar to main funtion)
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // set layout for this MainActivity
        setContentView(R.layout.main_activity);

        mMyoLockStateView = (TextView) findViewById(R.id.lock_state);
        mMyoStateView = (TextView) findViewById(R.id.text);

        // First, we initialize the Hub singleton with an application identifier.
        Hub hub = Hub.getInstance();
        if (!hub.init(this, getPackageName())) {
            // We can't do anything with the Myo device if the Hub can't be initialized, so exit.
            Toast.makeText(this, "Couldn't initialize Hub", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        // Next, register for DeviceListener callbacks.
        hub.addListener(mListener);

        // load last settings values which are stored in internal device storage
        SharedPreferences settings = getPreferences(0);

        ((CheckBox) findViewById(R.id.checkBox)).setChecked(settings.getBoolean("enableAdvancedMode", true));
        ((CheckBox) findViewById(R.id.checkBox2)).setChecked(settings.getBoolean("enableExperimentalMode", false));
        ((EditText) findViewById(R.id.topicName)).setText(settings.getString("topicName", "/cmd_vel"), TextView.BufferType.EDITABLE);
        ((EditText) findViewById(R.id.editTextMs)).setText(String.valueOf(settings.getLong("sleepTime", 30)), TextView.BufferType.EDITABLE);
        ((EditText) findViewById(R.id.editTextLinearVelocityStep)).setText(settings.getString("linearVelocity", "0.8"), TextView.BufferType.EDITABLE);
        ((EditText) findViewById(R.id.editTextAngularVelocityStep)).setText(settings.getString("angularVelocity", "0.4"), TextView.BufferType.EDITABLE);

        // initialize state variables and linear and angular velocity
        SettingsSingleton.getInstance().sleepTime =  Long.parseLong(((EditText) findViewById(R.id.editTextMs)).getText().toString());
        this.basicLinearVelocity = Double.parseDouble(((EditText) findViewById(R.id.editTextLinearVelocityStep)).getText().toString());;
        this.basicAngularVelocity = Double.parseDouble(((EditText) findViewById(R.id.editTextAngularVelocityStep)).getText().toString());;
        this.advancedControlModeEnabled = ((CheckBox) findViewById(R.id.checkBox)).isChecked();
        this.experimentalControlModeEnabled = ((CheckBox) findViewById(R.id.checkBox2)).isChecked();

    }

    /**
     * Classic and basic callback function onDestroy which is called at end of android application
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        // We don't want any callbacks when the Activity is gone, so unregister the listener.
        Hub.getInstance().removeListener(mListener);

        if (isFinishing()) {
            // The Activity is finishing, so shutdown the Hub. This will disconnect from the Myo.
            Hub.getInstance().shutdown();
        }
    }

    /**
     *  Not so important function callback
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    /**
     * Not so important function callback
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    /**
     * If user click to appropriate button, we will launch the ScanActivity to scan for Myos to connect to.
     */
    private void onScanActionSelected() {
        Intent intent = new Intent(this, ScanActivity.class);
        startActivity(intent);
    }

    /**
     * If user click to appropriate button, we will start executing ROS node (which take care of sending control messages to robot)
     */
    public void startControlRobot(View view) {
        // get topic name, which was set in GUI by user
        EditText topicName = (EditText) findViewById(R.id.topicName);
        String topicNameText = topicName.getText().toString();

        // save topic name to internal device storage
        SharedPreferences settings = getPreferences(0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("topicName", topicNameText);

        // Commit the edits!
        editor.commit();

        // get settings like time interval of sending control messages, angular and linear velocity, which were set in GUI by user
        // and apply this settings
        EditText ms = (EditText) findViewById(R.id.editTextMs);
        EditText stepLinearVelocity = (EditText) findViewById(R.id.editTextLinearVelocityStep);
        EditText stepAngularVelocity = (EditText) findViewById(R.id.editTextAngularVelocityStep);

        try {
            SettingsSingleton.getInstance().sleepTime = Long.parseLong(ms.getText().toString());
            if (SettingsSingleton.getInstance().sleepTime <= 0) {
                throw new Exception("Number of [ms] must be positive number");
            }
        }
        catch(Exception e) {
            Toast.makeText(this, "Error. Number of [ms] must be positive number", Toast.LENGTH_LONG).show();
            return;
        }

        try {
            this.basicLinearVelocity = Double.parseDouble(stepLinearVelocity.getText().toString());
            if (this.basicLinearVelocity <= 0.0) {
                throw new Exception("Linear velocity must be positive number");
            }
        }
        catch(Exception e) {
            Toast.makeText(this, "Error. Linear velocity must be positive number", Toast.LENGTH_LONG).show();
            return;
        }

        try {
            this.basicAngularVelocity = Double.parseDouble(stepAngularVelocity.getText().toString());
            if (this.basicAngularVelocity <= 0.0) {
                throw new Exception("Angular velocity must be positive number");
            }
        }
        catch(Exception e) {
            Toast.makeText(this, "Error. Angular velocity must be positive number", Toast.LENGTH_LONG).show();
            return;
        }

        // create ROS node instance which will send control message to robot
        rosMessageSenderNode = new ROSMessageSenderNode(topicNameText, this);

        NodeConfiguration nodeConfiguration;
        // OBSOLETTE: NodeConfiguration nodeConfiguration = NodeConfiguration.newPrivate();
        
        // create configuration for created ROS node instance
        try {
            nodeConfiguration = NodeConfiguration.newPublic(InetAddressFactory.newNonLoopback().getHostAddress(), getMasterUri());
        }
        catch(Exception e) {
            Toast.makeText(this, "Error. Please establish a Wi-fi connection between the robot and android device", Toast.LENGTH_LONG).show();
            return;
        }
        // check if user has set master URI, if no, launch appropriate activity where user can fill reuired master URI
        if (getMasterUri() == null) {
            Toast.makeText(this, "Error. Please set a valid master URI", Toast.LENGTH_LONG).show();
            super.startMasterChooser();
            return;
        }
        
        // set master uri in ROS node configuration
        nodeConfiguration.setMasterUri(getMasterUri());
        
        // finally, start executing ROS node instance
        try {
            ROSnodeMainExecutor.execute(rosMessageSenderNode, nodeConfiguration);
        }
        catch(Exception e) {
            Toast.makeText(this, "Error. Please set a valid master URI", Toast.LENGTH_LONG).show();
            super.startMasterChooser();
            return;
        }

    }

    /**
     * If user click to appropriate button, we will launch the MasterChooserActivity, which allow user to set MasterUri
     */
    public void starMasterChooser() {
        super.nodeMainExecutorService.setMasterUri(null);
        super.startMasterChooser();
    }

    /**
     * If user click to appropriate button, we apply settings which were changed in GUI by user (like time between sending control message, basic linear velocity, basic angular velocity)
     */
    public void changeSettings(View view) {

        // get settings values from GUI
        EditText ms = (EditText) findViewById(R.id.editTextMs);
        EditText stepLinearVelocity = (EditText) findViewById(R.id.editTextLinearVelocityStep);
        EditText stepAngularVelocity = (EditText) findViewById(R.id.editTextAngularVelocityStep);

        // try apply settings values from GUI 
        try {
            SettingsSingleton.getInstance().sleepTime = Long.parseLong(ms.getText().toString());
            if (SettingsSingleton.getInstance().sleepTime <= 0) {
                throw new Exception("Number of [ms] must be positive number");
            }
        }
        catch(Exception e) {
            Toast.makeText(this, "Error. Number of [ms] must be positive number", Toast.LENGTH_LONG).show();
            return;
        }

        try {
            this.basicLinearVelocity = Double.parseDouble(stepLinearVelocity.getText().toString());
            if (this.basicLinearVelocity <= 0.0) {
                throw new Exception("Linear velocity must be positive number");
            }
        }
        catch(Exception e) {
            Toast.makeText(this, "Error. Linear velocity must be positive number", Toast.LENGTH_LONG).show();
            return;
        }

        try {
            this.basicAngularVelocity = Double.parseDouble(stepAngularVelocity.getText().toString());
            if (this.basicAngularVelocity <= 0.0) {
                throw new Exception("Angular velocity must be positive number");
            }
        }
        catch(Exception e) {
            Toast.makeText(this, "Error. Angular velocity must be positive number", Toast.LENGTH_LONG).show();
            return;
        }

        // save changed settings to internal device storage
        SharedPreferences settings = getPreferences(0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putLong("sleepTime", Long.parseLong(ms.getText().toString()));
        editor.putString("linearVelocity", stepLinearVelocity.getText().toString());
        editor.putString("angularVelocity", stepAngularVelocity.getText().toString());

        // Commit the edits!
        editor.commit();

    }

    /**
     * If user click to appropriate checkbox, we enable advanced control mode (checkbox checked) or disable advanced control mode (checkbox unchecked)
     */
    public void onCheckboxClicked(View view) {
        // Is the view now checked?
        boolean checked = ((CheckBox) view).isChecked();
        this.advancedControlModeEnabled = checked;

        // save this new settings configuration
        SharedPreferences settings = getPreferences(0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean("enableAdvancedMode", checked);
        // Commit the edits!
        editor.commit();
    }

    /**
     * If user click to appropriate checkbox, we enable experimental control mode (checkbox checked) or disable advanced control mode (checkbox unchecked)
     */
    public void onCheckbox2Clicked(View view) {
        // Is the view now checked?
        boolean checked = ((CheckBox) view).isChecked();

        this.experimentalControlModeEnabled = checked;
        this.fistGesturePerformedWithinExperimentalControlMode = false;
        this.fingersSpreadGesturePerformedWithinExperimentalControlMode = false;

        // save this new settings configuration
        SharedPreferences settings = getPreferences(0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean("enableExperimentalMode", checked);
        // Commit the edits!
        editor.commit();
    }

    /**
     * If user click to appropriate button, we will call onScanActionSelected function, which launch the ScanActivity to scan for Myos to connect to.
     */
    public void onScanMyoClicked(View view) {
        onScanActionSelected();
    }

    /**
     * If user click to appropriate button, we will call starMasterChooser function, which launch the MasterChooserActivity, which allow user to set MasterUri
     */
    public void onSetMasterURIClicked(View view) {
        starMasterChooser();
    }

}
