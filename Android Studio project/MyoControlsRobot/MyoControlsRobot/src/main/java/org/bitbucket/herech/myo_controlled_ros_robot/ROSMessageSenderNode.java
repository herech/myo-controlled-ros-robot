/**
 *  Copyright 2016 Jan Herec, herech@seznam.cz
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *         you may not use this file except in compliance with the License.
 *         You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *        distributed under the License is distributed on an "AS IS" BASIS,
 *        WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *        See the License for the specific language governing permissions and
 *        limitations under the License.
 */

/**
 *  Created by Jan Herec on 29.2.2016 within the bachelor thesis "Wearable Devices: Possibilities of Using and Applications"
 *  at Faculty of Information Technology, Brno University of Technology.
 *  Based on sample class Talker from author damonkohler@google.com (Damon Kohler). Distributed under the Apache License, Version 2.0.
 */

package org.bitbucket.herech.myo_controlled_ros_robot;

import android.graphics.Color;
import android.os.Handler;
import android.os.Looper;
import android.widget.TextView;
import android.widget.Toast;

import org.ros.concurrent.CancellableLoop;
import org.ros.namespace.GraphName;
import org.ros.node.AbstractNodeMain;
import org.ros.node.ConnectedNode;
import org.ros.node.Node;
import org.ros.node.topic.Publisher;

/**
 * Class represents node, which sends control message (containing linear and angular velocity) to ROS robot.
 * This node can be named as senderNode or publisherNode, because of sending/publishing control messages to another node running on ROS robot.
 */
public class ROSMessageSenderNode extends AbstractNodeMain {

    private String topicName; // name of topic on ROS robot on which will be published control messages
    private MainActivity act; // instance of MainActivity

    /**
     * This function returns name of this node (senderNode)
     */
    @Override
    public GraphName getDefaultNodeName() {
        return GraphName.of("Myo_armband_controlled_ROS_robot/ROSMessageSenderNode");
    }

    /**
     * Constructor initialize member fields
     */
    public ROSMessageSenderNode(String topicName, MainActivity actt) {
        super();
        this.topicName = topicName;
        this.act = actt;
        // OBSOLETTE: Toast.makeText(act, "Communication error, please set a valid master URI and topic name", Toast.LENGTH_LONG).show();

    }

    /**
     * In case of connection error (this running ROS node on android cannot connect to node on ROS node on robot) is called this function, typically when user set invalid master URI.
     * So user is asked to set valid master uri. This type of connection error can come into being only when WI-fi AP on android phone is active.
     */
    @Override
    public void onError(Node node, Throwable throwable) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(act, "Error. Please set a valid master URI.", Toast.LENGTH_LONG).show();
            }
        });
        act.runOnUiThread(new Runnable() {
            public void run() {
                act.starMasterChooser();
            }
        });
        // OBSOLETTE: node.shutdown();
        // OBSOLETTE: node.getScheduledExecutorService().shutdown();
    }

    /**
     * In case of no connection error (this running ROS node on android is connected to node on ROS node on robot) is called this function.
     * So inside this function we know, that between 2 nodes (publisher on android and receiver on ros robot) is established connection.
     */
    @Override
    public void onStart(final ConnectedNode connectedNode) {
        // publisher instance is created. We specify the type of published message: geometry_msgs.Twist and name of topic on ROS robot on which will be published control messages
        final Publisher<geometry_msgs.Twist> publisher = connectedNode.newPublisher(this.topicName, geometry_msgs.Twist._TYPE);

        // This CancellableLoop will be canceled automatically when the node shuts down.
        connectedNode.executeCancellableLoop(new CancellableLoop() {

            /**
             * Here we can initialize some values, for example set i = 0 and inside function loop() we can increment this variable
             */
            @Override
            protected void setup() {}

            /**
             * This function is called repeatedly (but no recursively).
             * Main purpouse of this function is sending control messages to ROS robot in interval specify by variable sleepTime from SettingsSingleton.
             */
            @Override
            protected void loop() throws InterruptedException {
                // create new control message type of Twist
                final geometry_msgs.Twist directionalTwistMessages = publisher.newMessage();

                // set linear velocity within Twist message
                geometry_msgs.Vector3 linearVelocityVector = connectedNode.getTopicMessageFactory().newFromType(geometry_msgs.Vector3._TYPE);
                linearVelocityVector.setX(SettingsSingleton.getInstance().linearVelocityX);
                linearVelocityVector.setY(0.0);
                linearVelocityVector.setZ(0.0);
                directionalTwistMessages.setLinear(linearVelocityVector);

                // set angular velocity within Twist message
                geometry_msgs.Vector3 angularVelocityVector = connectedNode.getTopicMessageFactory().newFromType(geometry_msgs.Vector3._TYPE);
                angularVelocityVector.setX(0.0);
                angularVelocityVector.setY(0.0);
                angularVelocityVector.setZ(SettingsSingleton.getInstance().angularVelocityZ);
                directionalTwistMessages.setAngular(angularVelocityVector);

                // try publish created Twist message to respective topic of ROS robot
                publisher.publish(directionalTwistMessages);

                    // If publisher (this android app) has not any receiver (on ROS robot) then user is informed about it.
                    // This state occurs when between node on android and on robot is established connection,
                    // but user set invalid name of topic on robot. THis application publish control messages to topic within node,
                    // so connection only to node is insufficient, we need fully functional and running topic within node.
                    // This state also occurs when connection between nodes is lost during publishing to topic.
                    if (!publisher.hasSubscribers()) {
                        act.runOnUiThread(new Runnable() {
                            public void run() {
                                // OBSOLETTE: Toast.makeText(act, "No subscriber, please set a valid master URI and topic name", Toast.LENGTH_SHORT).show();
                                // OBSOLETTE: act.starMasterChooser();
                                TextView textView8 = (TextView) act.findViewById(R.id.textView4);
                                textView8.setTextColor(Color.parseColor("#e53935"));
                                textView8.setText("Robot disconnected");
                                ((TextView) act.findViewById(R.id.textView5)).setText("");
                            }
                        });
                        // OBSOLETTE:  this.cancel();
                        // OBSOLETTE: connectedNode.shutdown();
                    }

                    // If publisher (this android app) has any receiver (on ROS robot) then user is informed about it and also is informed about current linear and current angular velocity,
                    // which are encapsulated in currently sended Twist message.
                    // This is expected and also required state, because our application can publish control messages to topic.
                    else {
                        act.runOnUiThread(new Runnable() {
                            public void run() {
                                TextView textView8 = (TextView) act.findViewById(R.id.textView4);
                                textView8.setTextColor(Color.parseColor("#00c853"));
                                textView8.setText("Robot connected");
                                ((TextView) act.findViewById(R.id.textView5)).setText("lin_vel=" + String.format("%.2f", directionalTwistMessages.getLinear().getX()) + ", ang_vel=" + String.format("%.2f", directionalTwistMessages.getAngular().getZ()));
                            }
                        });
                    }

                // Sleep thread for some time (sleepTime). Upon its expiry this function loop() will be called again and next mesage will be send to robot, and so on, and so on.
                // So robot receive every ms (defined by sleepTime) control message.
                Thread.sleep(SettingsSingleton.getInstance().sleepTime);
            }
        });
    }
}
